package com.wordleclone.ui;

import javax.swing.*;

public class WordleFrame extends JFrame {

    private WordlePanel panel;

    public void setPanel(WordlePanel panel) {
        this.panel = panel;
        this.add(this.panel);
    }
}
