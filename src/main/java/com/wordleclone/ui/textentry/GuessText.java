package com.wordleclone.ui.textentry;

import com.wordleclone.manager.GameManager;
import com.wordleclone.ui.button.PlayAgainButtonGrid;
import com.wordleclone.ui.guess.WordleWord;
import com.wordleclone.ui.keyboard.WordleKeyboard;
import com.wordleclone.ui.manager.GUIUtility;
import com.wordleclone.ui.manager.MainPanelManager;
import com.wordleclone.ui.message.GuessMessage;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Locale;

public class GuessText extends JTextField implements KeyListener {

    private String guess;
    private static GuessText guessText;

    public GuessText(String text) {
        setText(text != null ? text.toLowerCase(Locale.ROOT) : "");
        this.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent ke) {
                if ((ke.getKeyChar() == KeyEvent.VK_ENTER)) {
                    clearMessage();
                    if (GameManager.getInstance().getValidator().isWordValid(guess)) {
                        addGuess();
                        updateKeyboard();
                        if (GameManager.getInstance().getCorrectWord().equalsIgnoreCase(guess)) {
                            addCorrectMessage();
                            showPlayAgain();
                        }
                        else if (!GameManager.getInstance().getGuessManager().hasMoreGuesses()) {
                            addNoMoreGuessesMessage();
                            showPlayAgain();
                        }
                    }
                    else {
                        addBadInputMessage();
                    }
                    setText("");
                    GUIUtility.resizeFrame();
                }
            }
        });
        this.addKeyListener(this);
        this.setBounds(130,100,500, 50);//x axis, y axis, width, height
        guessText = this;
    }

    public static GuessText getInstance() {
        if (guessText == null) {
            guessText = new GuessText("");
        }
        return guessText;
    }

    private void addGuess() {
        GameManager.getInstance().getGuessManager().addGuess(guess);
        MainPanelManager.getPanel().add(new WordleWord(guess));
        MainPanelManager.getPanel().requestFocus();
    }

    private void updateKeyboard() {
        GameManager.getInstance().getKeyboard().updateGuess(guess);
        WordleKeyboard.getInstance().updateKeyboard(GameManager.getInstance().getKeyboard().printKeyboard());
    }

    private void addCorrectMessage() {
        GuessMessage.getInstance().setMessage("You got it right! The word was \"" + guess + "\"!");
    }

    private void addNoMoreGuessesMessage() {
        GuessMessage.getInstance().setMessage("No more guesses! The word was \"" + GameManager.getInstance().getCorrectWord() + "\"!");
    }

    private void addBadInputMessage() {
        GuessMessage.getInstance().setMessage("\"" + guess + "\" is invalid! Gotta be a 5-letter word!");
    }

    private void clearMessage() {
        GuessMessage.getInstance().setMessage("");
    }

    private void showPlayAgain() {
        PlayAgainButtonGrid grid = new PlayAgainButtonGrid();
        MainPanelManager.getPanel().add(grid.getPanel(), 1);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        JTextField field = (JTextField)e.getSource();
        guess = field.getText().toLowerCase(Locale.ROOT);
    }
}
