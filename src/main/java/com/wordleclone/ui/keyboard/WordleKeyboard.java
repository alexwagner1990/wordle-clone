package com.wordleclone.ui.keyboard;

import javax.swing.*;
import java.text.MessageFormat;

public class WordleKeyboard extends JLabel {

    private static WordleKeyboard wordleKeyboard;
    public WordleKeyboard(String keyboard) {
        setText("");
        wordleKeyboard = this;
    }

    public static WordleKeyboard getInstance() {
        if (wordleKeyboard == null) {
            wordleKeyboard = new WordleKeyboard("");
        }
        return wordleKeyboard;
    }

    public void updateKeyboard(String keyboard) {
        setText(MessageFormat.format("<html><b>{0}</b></html>", keyboard));
    }
}
