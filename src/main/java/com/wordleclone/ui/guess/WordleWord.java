package com.wordleclone.ui.guess;

import com.wordleclone.manager.GameManager;

import javax.swing.*;
import java.text.MessageFormat;

public class WordleWord extends JLabel {

    public WordleWord(String text) {
        setText(MessageFormat.format("<html><b>{0}</b></html>", GameManager.getInstance().getGuessPrinter().printGuessWithColors(text)));
    }
}
