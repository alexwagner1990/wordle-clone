package com.wordleclone.ui.button;

import javax.swing.*;
import java.awt.*;

public class PlayAgainButtonGrid extends JFrame {
    private static PlayAgainButtonGrid playAgainButtonGrid;
    private JPanel buttonPanel;

    public PlayAgainButtonGrid() {
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1,2));
//        buttonPanel.setMaximumSize(new Dimension(10, 10));
        buttonPanel.add(new PlayAgainButtonYes());
        buttonPanel.add(new PlayAgainButtonNo());
        buttonPanel.setVisible(true);
        playAgainButtonGrid = this;
    }

    public static PlayAgainButtonGrid getInstance() {
        if (playAgainButtonGrid == null) {
            playAgainButtonGrid = new PlayAgainButtonGrid();
        }
        return playAgainButtonGrid;
    }

    public JPanel getPanel() {
        return buttonPanel;
    }

    public void setVisible(boolean visible) {
        buttonPanel.setVisible(visible);
    }
}
