package com.wordleclone.ui.button;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlayAgainButtonNo extends JButton implements ActionListener {

    public PlayAgainButtonNo() {
        setText("Quit");
        setSize(50, 50);
        this.addActionListener(this::actionPerformed);
    }

    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
