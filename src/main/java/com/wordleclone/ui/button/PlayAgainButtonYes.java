package com.wordleclone.ui.button;

import com.wordleclone.Start;
import com.wordleclone.ui.WordleFrame;
import com.wordleclone.ui.manager.GUIUtility;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlayAgainButtonYes extends JButton implements ActionListener {

    public PlayAgainButtonYes() {
        setText("Play Again");
        setSize(50, 50);
        this.addActionListener(this::actionPerformed);
    }

    public void actionPerformed(ActionEvent e) {
        WordleFrame frame = GUIUtility.getFrame();
        frame.setVisible(false);
        frame.dispose();
        Start.gui();
    }
}
