package com.wordleclone.ui.message;

import javax.swing.*;

public class GuessMessage extends JLabel {

    private static GuessMessage guessMessage;

    public GuessMessage() {
        this("Type a 5-letter guess to get started!");
    }

    public GuessMessage(String message) {
        setText(message);
        guessMessage = this;
    }

    public static GuessMessage getInstance() {
        if (guessMessage == null) {
            guessMessage = new GuessMessage("");
        }
        return guessMessage;
    }

    public void setMessage(String message) {
        setText(message);
    }

    public void clearMessage() {
        setText("");
    }
}
