package com.wordleclone.ui.manager;

import com.wordleclone.ui.WordlePanel;

public class MainPanelManager {

    private static WordlePanel instance;

    private MainPanelManager() {}

    public static void setPanel(WordlePanel panel) {
        instance = panel;
    }

    public static WordlePanel getPanel() {
        if (instance == null) {
            instance = new WordlePanel();
        }
        return instance;
    }
}
