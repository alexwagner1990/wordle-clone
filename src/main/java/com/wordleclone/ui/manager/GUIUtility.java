package com.wordleclone.ui.manager;

import com.wordleclone.ui.WordleFrame;
import com.wordleclone.ui.WordlePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GUIUtility {

    private static Integer WIDTH = 10000;
    private static Integer HEIGHT = 200;
    private static JPanel gamePanel;
    private static WordleFrame f;

    public static WordlePanel defaultPanel() {
        WordlePanel panel = new WordlePanel();
        panel.setSize(WIDTH,HEIGHT);
        panel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        panel.setLayout(new GridLayout(0,1, 100,5));
        return panel;
    }

    public static void startFrame(WordlePanel p) {
        f = new WordleFrame();
        gamePanel = p;
        f.setTitle("Wordle Clone Don't Sue Me!");
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }
        });
        f.add(p);
        f.pack();
        f.setLocationByPlatform(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

    public static void resizeFrame() {
        if (f != null) {
            f.validate();
            f.repaint();
            f.pack();
        }
    }

    public static JPanel getGamePanel() {
        return gamePanel;
    }

    public static WordleFrame getFrame() {
        return f;
    }
}
