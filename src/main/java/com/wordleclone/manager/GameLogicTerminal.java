package com.wordleclone.manager;

import com.wordleclone.utility.WordGuessPrinter;
import com.wordleclone.word.Keyboard;
import com.wordleclone.word.WordValidator;

import java.util.Scanner;

public class GameLogicTerminal {

    boolean playingAgain = false;

    public void runGame() {
        playingAgain = false;
        GameManager.getInstance().initializeGame();
        WordValidator validator = GameManager.getInstance().getValidator();
        GuessManager guessManager = GameManager.getInstance().getGuessManager();
        Keyboard keyboard = GameManager.getInstance().getKeyboard();
        WordGuessPrinter guessPrinter = GameManager.getInstance().getGuessPrinter();
        String correctWord = GameManager.getInstance().getCorrectWord();

        Scanner scanner = new Scanner(System.in);
        String guess = "";

        System.out.println("Pick a 5 - Letter Word to get started! Or type \"quit\" to quit!");

        while (!guess.equalsIgnoreCase("quit")) {
            guess = scanner.nextLine();
            if ("quit".equalsIgnoreCase(guess)) {
                break;
            }
            if (validator.isWordValid(guess)) {
                guessManager.addGuess(guess);
                keyboard.updateGuess(guess);
                if (guessManager.isCorrectWord(guess)) {
                    System.out.println("You got it! The word was \"" + correctWord + "\"!");
                    playingAgain(scanner);
                    guess = "quit";
                } else {
                    if (!guessManager.hasMoreGuesses()) {
                        System.out.println("You Lose! Good Day Sir!");
                        System.out.println("The word was \"" + guessManager.getCorrectWord() + "\"!");
                        playingAgain(scanner);
                        guess = "quit";
                    } else {
                        for (int i = 0; i < guessManager.getGuesses().size(); i++) {
                            System.out.println(guessPrinter.printGuessWithColors(guessManager.getGuesses().get(i)));
                        }
                        System.out.println(keyboard.printKeyboard());
                        System.out.println("You have " + guessManager.remainingGuesses() + " guess" + (guessManager.remainingGuesses() > 1 ? "es" : "") + " remaining!");
                    }
                }
            } else {
                System.out.println(validator.getMessage());
            }
        }
        if (playingAgain) {
            runGame();
        }

    }

    private void playingAgain(Scanner scanner) {
        System.out.println("Play Again? (y/n)");
        playingAgain = scanner.nextLine().equalsIgnoreCase("y");
    }
}
