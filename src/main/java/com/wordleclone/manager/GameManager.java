package com.wordleclone.manager;

import com.wordleclone.utility.WordGuessPrinter;
import com.wordleclone.word.Keyboard;
import com.wordleclone.word.WordPicker;
import com.wordleclone.word.WordValidator;

public class GameManager {

    private static GameManager gameManager;
    private WordPicker picker;
    private WordValidator validator;
    private String correctWord;
    private Keyboard keyboard;
    private GuessManager guessManager;
    private WordGuessPrinter guessPrinter;

    public static GameManager getInstance() {
        if (gameManager == null) {
            gameManager = new GameManager();
        }
        return gameManager;
    }

    public void initializeGame() {
        picker = new WordPicker();
        guessManager = new GuessManager(picker.getPickedWord());
        validator = new WordValidator(picker.getWords());
        correctWord = picker.getPickedWord();
        keyboard = new Keyboard(correctWord);
        guessPrinter = new WordGuessPrinter(correctWord);
    }

    public void startGame() {
        GameLogicTerminal terminal = new GameLogicTerminal();
        terminal.runGame();
    }

    public void playGameUI() {
        initializeGame();
        guessPrinter.setUI(true);
        keyboard.setUI(true);
    }

    // GETTERS

    public WordPicker getPicker() {
        return picker;
    }

    public WordValidator getValidator() {
        return validator;
    }

    public String getCorrectWord() {
        return correctWord;
    }

    public void setCorrectWord(String correctWord) {
        this.correctWord = correctWord;
    }

    public Keyboard getKeyboard() {
        return keyboard;
    }

    public GuessManager getGuessManager() {
        return guessManager;
    }

    public WordGuessPrinter getGuessPrinter() {
        return guessPrinter;
    }
}
