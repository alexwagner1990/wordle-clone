package com.wordleclone.manager;

import java.util.ArrayList;
import java.util.List;

public class GuessManager {

    private int totalGuesses = 5;
    private String correctWord;
    private List<String> guesses = new ArrayList<>();

    public GuessManager(String correctWord) {
        this.correctWord = correctWord;
    }

    public int getTotalGuesses() {
        return totalGuesses;
    }

    public void setTotalGuesses(int totalGuesses) {
        this.totalGuesses = totalGuesses;
    }

    public String getCorrectWord() {
        return correctWord;
    }

    public void setCorrectWord(String correctWord) {
        this.correctWord = correctWord;
    }

    public List<String> getGuesses() {
        return guesses;
    }

    public void addGuess(String guess) {
        guesses.add(guess);
    }

    public boolean isCorrectWord(String userGuess) {
        return userGuess.equalsIgnoreCase(correctWord);
    }

    public boolean hasMoreGuesses() {
        if (totalGuesses <= guesses.size()) {
            return false;
        }
        return true;
    }

    public void resetGuesses() {
        guesses = new ArrayList<>();
    }

    public int remainingGuesses() {
        return totalGuesses - guesses.size();
    }

}
