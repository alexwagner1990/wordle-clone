package com.wordleclone.word;

import java.util.List;

public class WordValidator {

    private List<String> possibleWords;
    private String message;
    private String NOT_RIGHT_LENGTH = "The guess has to be 5 letters long!\n";
    private String INVALID_CHARACTERS = "The guess must contain only letters!\n";
    private String INVALID_WORD = "The guess must be a valid word!\n";

    public WordValidator(List<String> allPossibleWords) {
        possibleWords = allPossibleWords;
    }

    public boolean isWordValid(String word) {
        message = "";
        if (word.length() != 5) {
            message += NOT_RIGHT_LENGTH;
        }
        if (!word.matches("[a-zA-Z]+")) {
            message += INVALID_CHARACTERS;
        }
        if (!possibleWords.contains(word)) {
            message += INVALID_WORD;
        }
        return message.length() < 1;
    }

    public String getMessage() {
        return message;
    }
}
