package com.wordleclone.word;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class WordPicker {

    private List<String> words = new LinkedList<>();
    private String pickedWord;

    public WordPicker() {
        pickWord();
    }

    public String pickWord() {
        InputStream stream = getClass().getClassLoader().getResourceAsStream("words.txt");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                words.add(line);
            }
        } catch (IOException e) {
            System.out.println("UH OH ON FILE!");
        }
        int randomNum = ThreadLocalRandom.current().nextInt(0, words.size());
        pickedWord = words.get(randomNum);
        return pickedWord;
    }

    public String getPickedWord() {
        return pickedWord;
    }

    public List<String> getWords() { return words; }

}
