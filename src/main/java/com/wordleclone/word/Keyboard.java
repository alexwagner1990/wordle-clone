package com.wordleclone.word;

import java.util.HashSet;
import java.util.Set;

public class Keyboard {

    private String correctAnswer;
    private static Keyboard keyboard;
    private boolean isUI = false;
    private Set<Character> chosenLetters;
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public Keyboard(String correctAnswer) {
        setCorrectAnswer(correctAnswer);
        keyboard = this;
    }

    public static Keyboard getInstance() {
        return keyboard;
    }

    public void setUI(boolean ui) {
        this.isUI = ui;
    }

    public void updateGuess(String guess) {
        char[] letters = guess.toCharArray();
        for (int i = 0; i < letters.length; i++) {
            chosenLetters.add(Character.toUpperCase(letters[i]));
        }
    }

    public String printKeyboard() {
        String keyboard = "";
        for (char letter = 65; letter <= 90; letter++) {
            boolean chosen = chosenLetters.contains(letter);
            if (isUI) {
                keyboard +=  ("<font color='" + (chosen ? "black" : "gray") + "'> " + letter + " </font>");
            }
            else {
                keyboard += (chosen ? ANSI_WHITE : "") + " " + letter + (chosen ? ANSI_RESET : "");
            }
        }
        return keyboard;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
        chosenLetters  = new HashSet<>();
    }

}
