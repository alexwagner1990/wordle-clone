package com.wordleclone.utility;

public class WordGuessPrinter {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_WHITE = "\u001B[37m";

    private String correctWord;
    private boolean isUI = false;

    public WordGuessPrinter(String correctAnswer) {
        setCorrectWord(correctAnswer);
    }

    public void setCorrectWord(String correctWord) {
        this.correctWord = correctWord.toUpperCase();
    }

    public void setUI(boolean ui) {
        this.isUI = ui;
    }

    public String printGuessWithColors(String guess) {
        String printedAnswer = "";
        char[] guessArray = guess.toUpperCase().toCharArray();
        for (int i = 0; i < guessArray.length; i++) {
            printedAnswer += " ";
            printedAnswer += printChar(guessArray[i], i);
        }
        return printedAnswer;
    }

    private String printChar(char letter, int index) {
        if (correctWord.toCharArray()[index] == letter) {
            if (isUI) {
                return "<font color='green'>" + letter + "</font>";
            }
            return ANSI_GREEN + " " + letter + " " + ANSI_RESET;
        }
        else if (correctWord.contains(Character.toString(letter))) {
            if (isUI) {
                return "<font color='orange'>" + letter + "</font>";
            }
            return  ANSI_YELLOW + " " + letter + " " + ANSI_RESET;
        }
        else {
            if (isUI) {
                return "<font color='black'>" + letter + "</font>";
            }
            return ANSI_WHITE + " " + letter + " " + ANSI_RESET;
        }
    }
}
