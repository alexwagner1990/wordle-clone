package com.wordleclone.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class WordGeneration {

    public static void buildWords(int wordLength) throws Exception {
        char letter = 'a';
        letter: while(letter <= 122) {
            int pageNumber = 1;
            pageNumber: while(true) {
                Object[] args = {pageNumber, wordLength, letter};
                URL url = new URL(String.format("https://www.dictionary.com/e/crb-ajax/cached.php?page=%s&wordLength=%s&letter=%s&action=get_wf_widget_page&pageType=4&nonce=e475fd0113", args));
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("Accept", "application/json");
                con.setDoOutput(true);
                try(BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"))) {
                    pageNumber++;
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while ((responseLine = br.readLine()) != null) {
                        response.append(responseLine.trim());
                    }
                    System.out.println(response.toString());
                    if (response.toString().contains("\"success\":true")) {
                        String array = response.substring(response.toString().indexOf("[") + 1, response.toString().indexOf("]"));
                        array = array.replaceAll("\"", "");
                        String[] arrayPart = array.split(",");
                        for (int i = 0; i < arrayPart.length; i++) {
                            String filePart = arrayPart[i] + "\n";
                            writeFile(filePart);
                        }
                    }
                    else {
                        break pageNumber;
                    }
                }
            }
            letter++;
        }
    }

    public static void buildWords() throws Exception {
        buildWords(5);
    }

    public static void writeFile(String str, String fileName) {
        Path path = Paths.get("./src/main/resources/" + fileName);
        byte[] strToBytes = str.getBytes();
        try{
            Files.write(path, strToBytes, StandardOpenOption.APPEND);
        }
        catch (IOException e){
            System.out.println("OOPS!");
        }
    }

    public static void writeFile(String str) {
        writeFile(str, "words.txt");
    }
}
