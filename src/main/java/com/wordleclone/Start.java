package com.wordleclone;

import com.wordleclone.manager.GameManager;
import com.wordleclone.ui.WordlePanel;
import com.wordleclone.ui.keyboard.WordleKeyboard;
import com.wordleclone.ui.manager.GUIUtility;
import com.wordleclone.ui.manager.MainPanelManager;
import com.wordleclone.ui.message.GuessMessage;
import com.wordleclone.ui.textentry.GuessText;

import javax.swing.*;

public class Start {

    public static void main(String[] args) {
        if (args.length == 0) {
            SwingUtilities.invokeLater(() -> gui());
        }
        else {
            System.out.println("Welcome to Wordle!");
            GameManager gameManager = new GameManager();
            gameManager.startGame();
            System.out.println("Okay Bye!");
        }
    }

    public static void gui() {
        GameManager.getInstance().playGameUI();
        WordlePanel panel = GUIUtility.defaultPanel();

        GuessMessage message = new GuessMessage();
        panel.add(message, 0);

        WordleKeyboard keyboard = new WordleKeyboard("");
        panel.add(keyboard, 1);

        GuessText guessBox = new GuessText("");
        panel.add(guessBox, 2);

        MainPanelManager.setPanel(panel);

        GUIUtility.startFrame(panel);
    }

}
