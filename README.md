# Wordle Clone

This is a wordle clone I'm making for fun, made to be as basic as possible with as few dependencies as possible.

## Requirements

- Java (8+)
- Maven

## How to run
A JAR can be built using the Maven Shade plugin, with the maven package goal in terminal:
```
mvn package
```
The JAR will be created in the /target directory, and it can be ran without parameters:
```
java -jar target/wordle-clone-1.0.0-SNAPSHOT.jar
```


## Gameplay

It's Wordle. Letters that are green are in the right place, yellow are in the word but in the wrong place, and grey if it's not in the word. 

The keyboard will grey out letters that have already been chosen (but the user can still choose greyed-out words). 

Five guesses to deduce the word. Valid words must be chosen for each guess.

The possible words were scrapped from Dictionary.com, so some chosen answers may be pretty obscure. I plan to eventually generate different sets of words (and word lengths) to use in play, but for now the possible words for the game can be modified by editing the words.txt file in the src/main/resources directory.

## On the Horizon

- Make the word generation script more usable
- Custom wordle games (different numbers of letters, guesses, banned guesses, etc)
- Gramphics